FROM trafex/php-nginx:2.7.0

LABEL AUTHOR="psych0d0g"
LABEL version="1.0"
LABEL description="SFPG Docker Container"

USER root

# renovate: datasource=github-tags depName=subchen/frep extractVersion=^v(?<version>.*)$ versioning=loose
ENV FREP_VERSION=1.3.13

RUN apk update && apk upgrade 
RUN apk --update --no-cache add \
    bash \
    php81-exif \
    php81-gd \
    patch

ADD "https://github.com/subchen/frep/releases/download/v${FREP_VERSION}/frep-${FREP_VERSION}-linux-amd64" /usr/bin/frep
RUN chmod +x /usr/bin/frep

ADD https://sye.dk/sfpg/Single_File_PHP_Gallery_4.11.0.zip /tmp

RUN rm /var/www/html/* && unzip /tmp/Single_File_PHP_Gallery_4.11.0.zip index.php -d /var/www/html

RUN sed -i "s/'GALLERY_ROOT', '.\/'/'GALLERY_ROOT', '.\/galleries\/'/g" index.php

RUN chown nobody:nobody /var/www/html -R

RUN chmod 777 /tmp

USER nobody
